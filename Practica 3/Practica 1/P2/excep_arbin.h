#ifndef EXCEP_ARBIN_H
#define EXCEP_ARBIN_H

#include "excepcion.h"

using namespace std;

class PosicionArbolExcepcion: public Excepcion {
  public:
     PosicionArbolExcepcion(): Excepcion("La posicion del iterador en el arbol no permite realizar esa operacion") {};
};

#endif

