#ifndef EXCEP_ABB_H
#define EXCEP_ABB_H

#include "excepcion.h"

using namespace std;

class ArbolVacioExcepcion: public Excepcion {
  public:
     ArbolVacioExcepcion(): Excepcion("Arbol vacio") {};
};

#endif

