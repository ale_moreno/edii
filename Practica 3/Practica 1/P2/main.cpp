#include <iostream>
#include <cstdlib>
#include <queue>
#include "arbin.h"
#include "abb.h"
#include "NoHaySiguienteMayor.h"

// Recorridos

template <typename T>
void inorden(const Arbin<T>& a, const typename Arbin<T>::Iterador& r) {
    if (!r.arbolVacio()) {
        inorden(a, a.subIzq(r));
        cout << r.observar() << " ";
        inorden(a, a.subDer(r));
    }
}

template <typename T>
void preorden(const Arbin<T>& a, const typename Arbin<T>::Iterador& r) {
    if (!r.arbolVacio()) {
        cout << r.observar() << " ";
        preorden(a, a.subIzq(r));
        preorden(a, a.subDer(r));
    }
}

template <typename T>
void postorden(const Arbin<T>& a, const typename Arbin<T>::Iterador& r) {
    if (!r.arbolVacio()) {
        postorden(a, a.subIzq(r));
        postorden(a, a.subDer(r));
        cout << r.observar() << " ";
    }
}

template <typename T>
void anchura(const Arbin<T>& a) {
    if (!a.esVacio()) {
        queue<typename Arbin<T>::Iterador> c;
        typename Arbin<T>::Iterador ic = a.getRaiz();
        c.push(ic);
        while (!c.empty()) {
             ic = c.front();
             c.pop();
             cout << ic.observar() << " ";
             if (!a.subIzq(ic).arbolVacio())
                c.push(a.subIzq(ic));
             if (!a.subDer(ic).arbolVacio())
                c.push(a.subDer(ic));
        }
    }
}


/***************************************************************************/
/****************************** EJERCICIOS *********************************/
/***************************************************************************/
//Ejercicio 1

template <typename T>
int numHojas(const Arbin<T>& a){
    return numHojas(a,a.getRaiz());
}

template <typename T>
int numHojas(const Arbin <T>& a, const typename Arbin<T>::Iterador& r){
    if (!r.arbolVacio()) {
            if (a.subIzq(r).arbolVacio() && a.subDer(r).arbolVacio())
                return 1;
            else
                return numHojas(a,a.subIzq(r)) + numHojas(a,a.subDer(r));
    }
    else return 0;
}

/****************************************************************************/
template <typename T>
Arbin<T> simetrico(const Arbin <T>& a){
    Arbin<T> aux;
    return simetrico(a,a.getRaiz());
}

template <typename T>
Arbin<T> simetrico(const Arbin<T>& a, const typename Arbin<T>::Iterador& r){
    if(!r.arbolVacio()){
            return Arbin<T>(r.observar(),simetrico(a,a.subDer(r)), simetrico(a,a.subIzq(r)));
    }
    else
        return Arbin<T>();
}

/****************************************************************************/
template<typename T>
void recorridoZigzag( const Arbin<T>& a, char sentido){
    recorridoZigzag(a, a.getRaiz(),sentido);
}

template<typename T>
void recorridoZigzag( const Arbin<T>& a, const typename Arbin<T>::Iterador& r, char sentido){
    if (!r.arbolVacio()){
        cout << r.observar() << " ";
        if (sentido == 'I')
            recorridoZigzag(a,a.subIzq(r),'D');
        else
            recorridoZigzag(a,a.subDer(r),'I');
    }
}

/******************************************************************************/
//Ejercicio 4

template <typename T>
int numnodos(const Arbin<T>& a,const typename Arbin<T>::Iterador& r){

    if(r.arbolVacio()) return 0;
    else{
        if(a.subDer(r).arbolVacio() && a.subIzq(r).arbolVacio())
            return 1;
        else{
            return (numnodos(a,a.subIzq(r))+numnodos(a,a.subDer(r))+1);
        }
    }
}

template <typename T>
bool compensado(const Arbin<T>& a){
   return compensado(a,a.getRaiz());
}

template <typename T>
bool compensado(const Arbin<T>& a,const typename Arbin<T>::Iterador& r){
    if(!r.arbolVacio()){
        if(a.subIzq(r).arbolVacio() && a.subDer(r).arbolVacio())
            return true;
        else{
            if(abs(numnodos(a,a.subIzq(r)) - numnodos(a,a.subDer(r)))<=1)
                return (compensado(a,a.subIzq(r)) && compensado(a,a.subDer(r)));
            else return false;
        }
    }
    else return true;
}

/*****************************************************************************/
//Ejercicio 5
template <typename T>
void palabras(const Arbin<T>& a){
    queue<typename Arbin<T>::Iterador> c;
    palabras(a,a.getRaiz(),c);
}

template <typename T>
void palabras(const Arbin<T>& a, const typename Arbin<T>::Iterador& r, queue<typename Arbin<T>::Iterador> c){
    typename Arbin<T>::Iterador ic = r;
    if(!r.arbolVacio()){
        if(a.subDer(r).arbolVacio()&&a.subIzq(r).arbolVacio()){
            while (!c.empty()){
                ic = c.front();
                cout << ic.observar();
                c.pop();
            }
            cout<<r.observar()<<endl;
        }
        else{
            c.push(ic);
            palabras(a,a.subIzq(r),c);
            palabras(a,a.subDer(r),c);
        }
    }
}

/******************************************************************************/
//Ejercicio 6
template <typename T>
int siguienteMayor(const ABB<T>& a, int x) throw(NoHaySiguienteMayor)
{
	int sm=0;
   	siguienteMayor(a,a.getRaiz(),x,sm);
   	if(sm!=0)
   		return sm;
   	else throw(NoHaySiguienteMayor());
}

template <typename T>
void siguienteMayor(const ABB<T>& a, const typename ABB<T>::Iterador& r, int x, int& sm)throw(NoHaySiguienteMayor){
	if(!r.arbolVacio()){
		if(a.subDer(r).arbolVacio()&&a.subIzq(r).arbolVacio()){
			if(x<r.observar()) sm=r.observar();
		}
		else{
			if(x==r.observar()) {
				if(!a.subDer(r).arbolVacio())
					sm=minimoIzq(a,a.subDer(r));
			}
			else if(x < r.observar()){ 
				sm=r.observar();
				siguienteMayor(a,a.subIzq(r),x, sm);
			}
			else siguienteMayor(a,a.subDer(r),x, sm);
		}
	}
	else throw (NoHaySiguienteMayor());
}

template <typename T>
int minimoIzq(const ABB<T>& a, const typename ABB<T>::Iterador& r){
	if(a.subIzq(r).arbolVacio())
		return r.observar();
	else
		minimoIzq(a,a.subIzq(r));
}


/******************************************************************************/
//Ejercicio 7


/******************************************************************************/
//Ejercicio 8


/****************************************************************************/
/****************************************************************************/
int main(int argc, char *argv[])
{
    Arbin<char> A('t', Arbin<char>('m', Arbin<char>(),
                                        Arbin<char>('f', Arbin<char>(), Arbin<char>())),
                       Arbin<char>('k', Arbin<char>('d', Arbin<char>(), Arbin<char>()),
                                        Arbin<char>()));

    Arbin<char> B('t', Arbin<char>('n', Arbin<char>(),
                                        Arbin<char>('d', Arbin<char>('e', Arbin<char>(), Arbin<char>()),
                                                         Arbin<char>())),
                       Arbin<char>('m', Arbin<char>('f', Arbin<char>(), Arbin<char>()),
                                        Arbin<char>('n', Arbin<char>(), Arbin<char>())));

    Arbin<char> D('t', Arbin<char>('k', Arbin<char>('d', Arbin<char>(), Arbin<char>()),
                                        Arbin<char>()),
                       Arbin<char>('m', Arbin<char>(),
                                        Arbin<char>('f', Arbin<char>(), Arbin<char>())));

    Arbin<char> E('o', Arbin<char>('r', Arbin<char>(),
                                        Arbin<char>('o', Arbin<char>(), Arbin<char>())),
                       Arbin<char>('l', Arbin<char>('a', Arbin<char>(), Arbin<char>()),
                                        Arbin<char>('e', Arbin<char>(), Arbin<char>())));

    Arbin<int> F(2, Arbin<int>(7, Arbin<int>(2, Arbin<int>(), Arbin<int>()),
                                  Arbin<int>(6, Arbin<int>(5, Arbin<int>(), Arbin<int>()),
                                                Arbin<int>(11, Arbin<int>(), Arbin<int>()))),
                    Arbin<int>(5, Arbin<int>(),
                                  Arbin<int>(9, Arbin<int>(),
                                                  Arbin<int>(4, Arbin<int>(), Arbin<int>()))));

    ABB<int> BB6, BB7;



    // NUMERO HOJAS //
    cout << "Num. hojas del arbol B: " << numHojas(B) << endl;
    cout << "Num. hojas del arbol A: " << numHojas(A) << endl;
    cout << "Num. hojas del arbol E: " << numHojas(E) << endl << endl;

    // COPIA SIMETRICA //
    Arbin<char> C = simetrico(B);
    cout << "Recorrido en inorden del arbol B: " << endl;
    inorden(B, B.getRaiz());
    cout << endl << "Recorrido en inorden del arbol C: " << endl;
    inorden(C, C.getRaiz());
    cout << endl << endl;

    Arbin<int> X = simetrico(F);
    cout << "Recorrido en inorden del arbol F: " << endl;
    inorden(F, F.getRaiz());
    cout << endl << "Recorrido en inorden del arbol X: " << endl;
    inorden(X, X.getRaiz());
    cout << endl << endl;

    // RECORRIDO EN ZIG-ZAG //

    cout << "Recorrido en zigzag I de B:\n";
    recorridoZigzag(B, 'I');
    cout << endl;
    cout << "Recorrido en zigzag D de C:\n";
    recorridoZigzag(C, 'D');
    cout << endl << endl;


    // COMPENSADO //
    cout << "Esta A compensado?:";
    cout << (compensado(A) ? " SI" : " NO") << endl;
    cout << "Esta B compensado?:";
    cout << (compensado(B) ? " SI" : " NO") << endl;
    cout << "Esta E compensado?:";
    cout << (compensado(E) ? " SI" : " NO") << endl;
    cout << "Esta F compensado?:";
    cout << (compensado(F) ? " SI" : " NO") << endl << endl;

    // PALABRAS DE UN ARBOL //
    cout << "PALABRAS DE A:\n";
    palabras(E);
    cout << "PALABRAS DE B:\n";
    palabras(B);
    cout << "PALABRAS DE F:\n";
    palabras(F);
    cout << endl;

    // SIGUIENTE MAYOR
    BB6.insertar(8); BB6.insertar(3); BB6.insertar(10); BB6.insertar(1); BB6.insertar(6);
    BB6.insertar(14); BB6.insertar(4); BB6.insertar(7); BB6.insertar(13);
    try
    {
        cout << "Siguiente mayor en BB6 de 5: " << siguienteMayor(BB6, 5) << endl;
        cout << "Siguiente mayor en BB6 de 8: " << siguienteMayor(BB6, 8) << endl;
        cout << "Siguiente mayor en BB6 de 13: " << siguienteMayor(BB6, 13) << endl;
        cout << "Siguiente mayor en BB6 de 14: " << siguienteMayor(BB6, 14) << endl;
    }
    catch(const NoHaySiguienteMayor& e)
    {
        cout << e.Mensaje() << endl << endl;
    }
/*
    // POSICION INORDEN //
    BB7.insertar(5); BB7.insertar(1); BB7.insertar(3); BB7.insertar(8); BB7.insertar(6);
    cout << "Posicion Inorden en BB7 de 3: ";
    cout << posicionInorden(BB7, 3);
    cout << endl << "Posicion Inorden en BB7 de 8: ";
    cout << posicionInorden(BB7, 8);
    cout << endl << "Posicion Inorden en BB7 de 7: ";
    cout << posicionInorden(BB7, 7);
    cout << endl << endl;

    // SUMA CAMINO
    cout << "Hay un camino de suma 26 en F?:";
    cout << (haySumaCamino(F, 26) ? " SI" : " NO") << endl;
    cout << "Hay un camino de suma 9 en F?:";
    cout << (haySumaCamino(F, 9) ? " SI" : " NO") << endl;
*/

    system("PAUSE");
    return 0;
}
