#include "cartelera.h"
#include <iostream>

using namespace std;

Cartelera::Cartelera(): espectaculos()
{
}

void Cartelera::insertaEspectaculo(const string& e)
{
    espectaculos.insert(PEspectaculos(e,DSalas()));
}

void Cartelera::insertaSala(const string& e, const string& s, const string& c)
{
    espectaculos[e].insert(PSalas(s,c));
}

void Cartelera::eliminaEspectaculo(const string& e)
{
    espectaculos.erase(e);
}

void Cartelera::eliminaSala(const string& e, const string& s)
{
    DEspectaculos::iterator it;
    it=espectaculos.find(e);
    if(it!=espectaculos.end())
        it->second.erase(s);
}

void Cartelera::listaEspectaculos()
{
    if(!espectaculos.empty()){
        for(DEspectaculos::iterator it = espectaculos.begin(); it!=espectaculos.end(); it++){
            cout << it->first;
            cout << endl;
        }
    }
}

void Cartelera::listaSalas(const string& e)
{
   DEspectaculos::iterator it1;
   it1=espectaculos.find(e);
   if(it1!=espectaculos.end()){
     for(DSalas::iterator it = it1->second.begin(); it != it1->second.end(); it++){
        cout << "Sala: " << it->first << " Ciudad: " << it->second;
        cout << endl;
    }
   }
}

