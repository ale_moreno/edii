#include <iostream>
#include "Multiconjunto.h"
#include "Persona.h"

using namespace std;

int main()
{

    Persona p1;
    Multiconjunto<Persona> v1;

    if(v1.esVacio()) cout<<"Esta vacio el sistema"<<endl;

    p1.setNombre("pepe");p1.setEdad(19);
    v1.anade(p1);
    p1.setNombre("Antonio");p1.setEdad(19);
    v1.anade(p1);
    p1.setNombre("Julio");p1.setEdad(20);
    v1.anade(p1);
    p1.setNombre("Alejandro");p1.setEdad(23);
    v1.anade(p1);
    p1.setNombre("Antonio");p1.setEdad(19);
    v1.anade(p1);

    cout << "Numero de elementos: " << v1.cardinalidad()<<endl;

    if(v1.esVacio()) cout<<"Esta vacio el sistema"<<endl;
    else cout<<"No vacio"<<endl;

    p1.setNombre("pepe");p1.setEdad(19);
    v1.elimina(p1);

    cout << "Numero de elementos: " << v1.cardinalidad()<<endl; // 4 elementos

    return 0;
}
