#include "Persona.h"

 const string& Persona::getNombre()const{
    return nombre;
 }

 int Persona::getEdad()const{
    return edad;
 }

 void Persona::setNombre(const string& n){
    //strcpy(nombre,n);
    nombre=n;
 }

 void Persona::setEdad(int e){
    edad = e;
 }

 bool Persona::operator==(const Persona& p)const{

    if(p.getNombre()==nombre && p.getEdad()== edad) return true;
    else return false;
 }
