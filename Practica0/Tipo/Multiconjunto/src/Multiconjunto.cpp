#include "Multiconjunto.h"


template <typename T>
Multiconjunto<T>::Multiconjunto()
{
    num=0;
}

template <typename T>
bool Multiconjunto<T>::esVacio() const
{
    return (num!=0 ? false : true);
}

template <typename T>
int Multiconjunto<T>::cardinalidad()const
{
    return num;
}

template <typename T>
void Multiconjunto<T>::anade(const T& objeto)
{
  c[num]=objeto;
  num++;
}

template <typename T>
void Multiconjunto<T>::elimina(const T& objeto)
{
    if(pertenece(objeto))
    {
        for(int i = 0; i<num; i++)
        {
            if(c[i]==objeto)
            {
                if(i==num-1)num--;
                else{
                    c[i]=c[num-1];
                    num--;
                }
            }
        }
        if(pertenece(objeto)) elimina(objeto);
    }
}

template <typename T>
bool Multiconjunto<T>::pertenece(const T& objeto)const
{
        int i = 0;
        bool found=false;
        while (/*c[i]!=objeto*/!found && i< num)
        {
            if(c[i]==objeto) found=true;
            else i++;
        }
        return (i<num ? true : false);

}

template class Multiconjunto<int>;
template class Multiconjunto<char>;
template class Multiconjunto<Persona>;
