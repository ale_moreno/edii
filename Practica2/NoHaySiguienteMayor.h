#ifndef NOHAYSIGUIENTEMAYOR_H
#define NOHAYSIGUIENTEMAYOR_H

#include "excepcion.h"

using namespace std;

class NoHaySiguienteMayor: public Excepcion {
  public:
     NoHaySiguienteMayor(): Excepcion("El arbol no tiene un elemento mayor al proporcionado") {};
};

#endif