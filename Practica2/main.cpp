#include <iostream>
#include <cstdlib>
#include <queue>
#include <list>
#include "arbin.h"
#include "abb.h"
#include "NoHaySiguienteMayor.h"
#include <sstream>

// Recorridos

template <typename T>
void inorden(const Arbin<T>& a, const typename Arbin<T>::Iterador& r) {
    if (!r.arbolVacio()) {
        inorden(a, a.subIzq(r));
        cout << r.observar() << " ";
        inorden(a, a.subDer(r));
    }
}

template <typename T>
void preorden(const Arbin<T>& a, const typename Arbin<T>::Iterador& r) {
    if (!r.arbolVacio()) {
        cout << r.observar() << " ";
        preorden(a, a.subIzq(r));
        preorden(a, a.subDer(r));
    }
}

template <typename T>
void postorden(const Arbin<T>& a, const typename Arbin<T>::Iterador& r) {
    if (!r.arbolVacio()) {
        postorden(a, a.subIzq(r));
        postorden(a, a.subDer(r));
        cout << r.observar() << " ";
    }
}

template <typename T>
void anchura(const Arbin<T>& a) {
    if (!a.esVacio()) {
        queue<typename Arbin<T>::Iterador> c;
        typename Arbin<T>::Iterador ic = a.getRaiz();
        c.push(ic);
        while (!c.empty()) {
             ic = c.front();
             c.pop();
             cout << ic.observar() << " ";
             if (!a.subIzq(ic).arbolVacio())
                c.push(a.subIzq(ic));
             if (!a.subDer(ic).arbolVacio())
                c.push(a.subDer(ic));
        }
    }
}


/***************************************************************************/
/****************************** EJERCICIOS *********************************/
/***************************************************************************/

///****************************************************************************/
///**Ejercicio numero de hojas**/
//
//template <typename T>
//int numHojas(const Arbin<T>& a){
//    return numHojas(a,a.getRaiz());
//}
//
//template <typename T>
//int numHojas(const Arbin<T>& a, const typename Arbin<T>::Iterador& r){
//    if(!r.arbolVacio()){
//        if(a.subDer(r).arbolVacio() && a.subIzq(r).arbolVacio()) return 1;
//        else{
//            return numHojas(a,a.subDer(r))+numHojas(a,a.subIzq(r));
//        }
//    }
//    else return 0;
//}
//
//
///****************************************************************************/
///**Ejercicio copia simetrica**/
//
//template <typename T>
//Arbin<T> simetrico (const Arbin<T>& a){
//    return simetrico(a,a.getRaiz());
//}
//
//template <typename T>
//Arbin<T> simetrico(const Arbin<T>& a, const typename Arbin<T>::Iterador& r){
//    if(!r.arbolVacio())
//        return Arbin<T>(r.observar(), simetrico(a,a.subDer(r)), simetrico(a,a.subIzq(r)));
//    else
//       return Arbin<T>();
//}
//
///****************************************************************************/
///**Ejercicio zigzag**/
//
//template <typename T>
//void recorridoZigzag(const Arbin<T>& a, char sentido){
//    if(sentido == 'I')
//        recorridoZigzag(a,a.getRaiz(),'D');
//    else
//        recorridoZigzag(a,a.getRaiz(),'I');
//}
//
//template <typename T>
//void recorridoZigzag(const Arbin<T>& a, const typename Arbin<T>::Iterador& r, char sentido){
//    if(!r.arbolVacio()){
//        cout << r.observar();
//        if(sentido=='I')
//            recorridoZigzag(a,a.subIzq(r),'D');
//        else
//            recorridoZigzag(a,a.subDer(r),'I');
//    }
//}
//
///****************************************************************************/
///**Ejercicio arbol compensado**/
//
//template <typename T>
//bool compensado(const Arbin<T>& a){
//    return compensado(a,a.getRaiz());
//}
//
//template <typename T>
//bool compensado(const Arbin<T>& a, const typename Arbin<T>::Iterador& r){
//    if(r.arbolVacio()) return true;
//    else{
//        if(abs(numnodos(a,a.subIzq(r))-numnodos(a,a.subDer(r)))<=1){
//            return compensado(a,a.subIzq(r))&&compensado(a,a.subDer(r));
//        }
//        return false;
//    }
//}
//
//template <typename T>
//int numnodos(const Arbin<T>& a, const typename Arbin<T>::Iterador& r){
//    if(r.arbolVacio()) return 0;
//    else{
//        return numnodos(a,a.subDer(r))+numnodos(a,a.subIzq(r))+1;
//    }
//}
//
///****************************************************************************/
///**Ejercicio palabras de un arbol**/
//
//template <typename T>
//void palabras(const Arbin<T>& a){
//    queue<T> cola;
//    palabras(a,a.getRaiz(),cola);
//}
//
//template <typename T>
//void palabras(const Arbin<T>& a, const typename Arbin<T>::Iterador& r, queue<T> cola){
//    if(!r.arbolVacio()){
//        cola.push(r.observar());
//        if(a.subIzq(r).arbolVacio()&&a.subDer(r).arbolVacio()){
//            while(!cola.empty()){
//                cout << cola.front();
//                cola.pop();
//            }
//            cout << endl;
//        }
//        else{
//            palabras(a,a.subIzq(r),cola);
//            palabras(a,a.subDer(r),cola);
//        }
//    }
//}
//
///****************************************************************************/
///**Ejercicio siguiente mayor**/
//
//template <typename T>
//int siguienteMayor(const ABB<T>& a, int numero)throw (NoHaySiguienteMayor){
//    int sm = 0;
//    siguienteMayor(a,a.getRaiz(), numero, sm);
//    if(sm==0) throw(NoHaySiguienteMayor());
//    else return sm;
//}
//
//template <typename T>
//void siguienteMayor(const ABB<T>& a, const typename ABB<T>::Iterador& r, int numero, int& sm){
//    if(!r.arbolVacio()){
//        if(r.observar()==numero){
//            if(!a.subDer(r).arbolVacio()){
//                sm=minimo(a,a.subDer(r));
//            }
//        }
//        else{
//            if(r.observar()>numero){
//                sm=r.observar();
//                siguienteMayor(a,a.subIzq(r),numero,sm);
//            }
//            else{
//                siguienteMayor(a,a.subDer(r),numero,sm);
//            }
//        }
//    }
//}
//
//template <typename T>
//int minimo(const ABB<T>& a, const typename ABB<T>::Iterador& r){
//    if(!r.arbolVacio()){
//        if(a.subIzq(r).arbolVacio())
//            return r.observar();
//        else
//            return minimo(a,a.subIzq(r));
//    }
//}
//
///****************************************************************************/
///**Ejercicio Posici�n Inorden**/
//
//template <typename T>
//int posicionInorden(const ABB<T>& a, int numero){
//    bool encontrado = false;
//    int pos=posicionInorden(a,a.getRaiz(),numero, encontrado);
//    if(encontrado)
//        return pos;
//    else return 0;
//}
//
//template <typename T>
//int posicionInorden(const ABB<T>& a, const typename ABB<T>::Iterador& r, int numero, bool& encontrado){
//    if(r.arbolVacio()) return 0;
//    else{
//        if(r.observar()==numero){
//            encontrado=true;
//            return numnodos(a,a.subIzq(r))+1;
//        }
//        else if(r.observar()>numero)
//            return posicionInorden(a,a.subIzq(r),numero,encontrado);
//        else
//            return numnodos(a,a.subIzq(r))+1+posicionInorden(a,a.subDer(r),numero,encontrado);
//    }
//}
//
///****************************************************************************/
///**Ejercicio suma del camino**/
//
//template <typename T>
//bool haySumaCamino(const Arbin<T>& a, int suma){
//    return haySumaCamino(a,a.getRaiz(),suma);
//}
//
//template <typename T>
//bool haySumaCamino(const Arbin<T>& a, const typename Arbin<T>::Iterador& r, int suma){
//    if(!r.arbolVacio()){
//        suma=suma-r.observar();
//        if(a.subDer(r).arbolVacio()&&a.subIzq(r).arbolVacio()){
//            if(suma==0) return true;
//            else return false;
//        }
//        else{
//            return (haySumaCamino(a,a.subIzq(r),suma) || haySumaCamino(a,a.subDer(r),suma));
//        }
//
//    }
//}

//*********************************************************************************//
//EJERCICIOS PARA EL EXAMEN

/*Ejercicio 1*/
///Dado un �rbol binario, escribir una funci�n que devuelva el n�mero de hojas de dicho �rbol.
template<typename T>
int numHojas(const Arbin<T>& A){
    return numHojas(A,A.getRaiz());
}

template <typename T>
int numHojas(const Arbin<T>& A, const typename Arbin<T>::Iterador &r){
    if(!r.arbolVacio()){
        if(A.subDer(r).arbolVacio() && A.subIzq(r).arbolVacio())
            return 1;
        else
            return (numHojas(A,A.subDer(r))+numHojas(A,A.subIzq(r)));
    }
    else return 0;
}


///Ejercicio 2
///Dise�ar una funci�n que, dado un �rbol binario A, devuelva una copia sim�trica del mismo.
///Para el �rbol A de la Figura 1, el �rbol sim�trico que devolver�a la funci�n ser�a el �rbol B de la Figura 1.

template<typename T>
Arbin<T> simetrico(const Arbin<T>& a){
    return simetrico(a,a.getRaiz());
}

template <typename T>
Arbin<T> simetrico(const Arbin<T>& a, const typename Arbin<T>::Iterador& r){
    if(r.arbolVacio())
        return  Arbin<T>();
    else{
        return Arbin<T>(r.observar(), simetrico(a,a.subDer(r)), simetrico(a,a.subIzq(r)));
    }
}

///Ejercicio 3
///Realizar un procedimiento que a partir de un �rbol binario, muestre por pantalla los
///elementos de los nodos visitados mediante un recorrido en zigzag, comenzando por
/// un sentido dado (�I� para sub�rbol izquierdo, �D� para sub�rbol derecho):

template <typename T>
void recorridoZigzag( const Arbin<T>& a, char sentido ){
    recorridoZigzag(a,a.getRaiz(),sentido);
}

template <typename T>
void recorridoZigzag(const Arbin<T>& a, const typename Arbin<T>::Iterador& r, char sentido){
    if(!r.arbolVacio()){
        cout << r.observar() << " ";
        if(sentido == 'I')
            recorridoZigzag(a,a.subIzq(r),'D');
        else
             recorridoZigzag(a,a.subDer(r),'I');
    }
}

///Ejercicio 4
///Un �rbol binario est� compensado si, para cada nodo interno del �rbol, la diferencia entre el n�mero de nodos
///de sus hijos es menor o igual que 1. Escribir una funci�n que devuelva verdadero si un �rbol est� compensado
///y falso en caso contrario. Se entiende que el �rbol vac�o y las hojas est�n compensados
template <typename T>
bool compensado(const Arbin<T>& a){
    return (compensado(a,a.getRaiz()));
}

template <typename T>
bool compensado(const Arbin<T>& a, const typename Arbin<T>::Iterador& r){
    if(!r.arbolVacio()){
            int x = numnodos(a,a.subDer(r))-numnodos(a,a.subIzq(r));
            if(x<=1 && x>=-1)
                return (compensado(a,a.subDer(r))&&compensado(a,a.subIzq(r)));
            else
                return false;
    }
    else
        return true;
}

template<typename T>
int numnodos(const Arbin<T>& a, const typename Arbin<T>::Iterador& r){
    if(!r.arbolVacio()){
        return numnodos(a,a.subIzq(r))+numnodos(a,a.subDer(r))+1;
    }
    else
        return 0;
}

///Ejercicio 5
///Cada nodo de un �rbol binario A almacena una letra. La concatenaci�n de las mismas,
///en cada camino que va desde la ra�z a una hoja representa una palabra. Realizar una
/// funci�n que visualice todas las palabras almacenadas en un �rbol binario A.
///Por ejemplo, para el �rbol de la Figura 2 deber�an visualizarse las siguientes palabras: oro, ola y ol�.

template <typename T>
void palabras(const Arbin<T>& a){
    string s = " ";
    palabras(a,a.getRaiz(),s);
}

template <typename T>
void palabras(const Arbin<T>& a, const typename Arbin<T>::Iterador& r, string s){
    stringstream ss;
    if(!r.arbolVacio()){
        if(a.subDer(r).arbolVacio() && a.subIzq(r).arbolVacio())
            cout << s << r.observar() << endl;
        else{
            ss<<s;
            ss<<r.observar();
            palabras(a,a.subDer(r),ss.str());
            palabras(a,a.subIzq(r),ss.str());
        }
    }
}

///Ejercicio 6
///Sea a un �rbol de b�squeda binario de enteros positivos y x un n�mero entero positivo.
///Se pide escribir una funci�n que dados a y x devuelva el siguiente elemento mayor que
/// x almacenado en el �rbol. En caso de que x no tenga un elemento mayor en el �rbol a debe
///lanzar una excepci�n. El elemento x puede estar en el �rbol o no.
///int siguienteMayor(const ABB<int>& a, int x) throw (NoHaySiguienteMayor)
///Figura 3. �rbol binario de b�usqueda de enteros positivos
///En el �rbol de la Figura 3, la llamada siguienteMayor(a, 5) debe devolver 6. Sin embargo,
///para la llamada siguienteMayor(a, 14) debe lanzar la excepci�n noHaySiguienteMayor.

int menor(const ABB<int>& a, const typename ABB<int>::Iterador& r){
    if(!r.arbolVacio()){
        if(a.subIzq(r).arbolVacio())
            return r.observar();
        else
            return menor(a,a.subIzq(r));
    }
}

void siguienteMayor(const ABB<int>& a, const typename ABB<int>::Iterador& r, int x, int& sm) {
  if(!r.arbolVacio()){
     if(r.observar()==x){
        if(!a.subDer(r).arbolVacio())
            sm = menor(a,a.subDer(r));
        }
    else if(r.observar()<x)
         siguienteMayor(a,a.subDer(r),x,sm);
    else{
        sm=r.observar();
        siguienteMayor(a,a.subIzq(r),x,sm);
    }
  }
}

int siguienteMayor(const ABB<int>& a, int x) throw(NoHaySiguienteMayor){
    int sm = 0;
    siguienteMayor(a,a.getRaiz(),x,sm);
    if(sm!=0)
        return sm;
    else
        throw(NoHaySiguienteMayor());
}

///Ejercicio 7
///Realizar una funci�n que devuelva la posici�n de un elemento en un ABB
///(desde 1 hasta el n�mero de nodos del �rbol) seg�n un recorrido en inorden de dicho �rbol.
///Si el elemento no est� en el �rbol, devolver� 0.

template<typename T>
int posicionInorden(const ABB<T>& a, int x){
    return posicionInorden(a,a.getRaiz(),x);
}

template<typename T>
int posicionInorden(const ABB<T>& a, const typename ABB<T>::Iterador& r, int x){
    if(r.arbolVacio()) return 0;
    else{
        if(r.observar()==x)
            return numnodos(a,a.subIzq(r))+1;
        else if(r.observar()>x)
            return posicionInorden(a,a.subIzq(r),x);
        else{
            int pos = posicionInorden(a,a.subDer(r),x);
            if(pos!=0)
                return pos+1+numnodos(a,a.subIzq(r));
            else
                return pos;
        }
    }
}

///Ejercicio 8
///Sea a un �rbol binario de enteros. Denominamos suma de un camino a la suma de los valores
///de los nodos que forman un camino que vaya desde la ra�z del �rbol a una de sus hojas. Se pide
///escribir una funci�n booleana que dado a y un valor entero devuelva cierto si dicho valor coincide
///con alguna suma de camino en dicho �rbol y falso si no coincide con ninguna. Para un �rbol vac�o
///la funci�n haySumaCamino devolver� cierto para un valor de suma 0 y falso en cualquier otro caso.

bool haySumaCamino(const Arbin<int>& a, const typename Arbin<int>::Iterador& r, int suma){
    if(!r.arbolVacio()){
        if(a.subDer(r).arbolVacio() && a.subIzq(r).arbolVacio()){
            if(suma==r.observar()) return true;
            else return false;
        }
        else{
            int resto = suma-r.observar();
            return (haySumaCamino(a,a.subDer(r),resto) || haySumaCamino(a,a.subIzq(r),resto));
        }
    }
    else return false;
}

bool haySumaCamino(const Arbin<int>& a, int suma){
    return haySumaCamino(a,a.getRaiz(),suma);
}


///**********************Ejercicio Boletin**********************************///

///Ejercicio 1
template <typename T>
bool identicos(const Arbin<T>& a, const Arbin<T>& b){
    return identicos(a,b,a.getRaiz(),b.getRaiz());
}

template <typename T>
bool identicos(const Arbin<T>& a, const Arbin<T>& b, const typename Arbin<T>::Iterador& ra,  const typename Arbin<T>::Iterador& rb){
    if(!ra.arbolVacio() && !rb.arbolVacio()){
        if(ra.observar() == rb.observar()){
           return  identicos(a,b,a.subDer(ra),b.subDer(rb)) &&  identicos(a,b,a.subIzq(ra),b.subIzq(rb));
        }
        else return false;
    }
    else if(ra.arbolVacio() && rb.arbolVacio()) return true;
    else return false;
}

///Ejercicio 2

template <typename T>
int sumanodos(const Arbin<T>& a){
    return sumanodos(a,a.getRaiz());
}

template <typename T>
int sumanodos(const Arbin<T>& a, const typename Arbin<T>::Iterador& r){
    if(r.arbolVacio()) return 0;
    else{
        if(a.subDer(r).arbolVacio() && a.subIzq(r).arbolVacio()) return r.observar();
        else{
            return r.observar()+sumanodos(a,a.subDer(r))+sumanodos(a,a.subIzq(r));
        }
    }
}

///Ejercicio 4

template <typename T>
void nivel(const Arbin<T>& a, int n){
    nivel(a,a.getRaiz(),n);
}

template <typename T>
void nivel(const Arbin<T>& a, const typename Arbin<T>::Iterador& r, int n){
    if(!r.arbolVacio() && n >=0){
        if(n == 0) cout << r.observar() << " ";
        else{
            n--;
            nivel(a,a.subIzq(r),n);
            nivel(a,a.subDer(r),n);
        }
    }
}

///Ejercicio 5

template <typename T>
bool completo(const Arbin<T>& a){
    return completo(a,a.getRaiz(),a.getRaiz().altura());
}

template <typename T>
bool completo(const Arbin<T>& a, const typename Arbin<T>::Iterador& r,int altura){
  if(r.arbolVacio()) return true;
  else{
    if(a.subDer(r).arbolVacio() && a.subIzq(r).arbolVacio()){
        if(altura-1 == 0 ) return true;
        else return false;
    }
    else if(a.subDer(r).arbolVacio() || a.subIzq(r).arbolVacio()) return false;
    else{
        altura--;
        return completo(a,a.subDer(r),altura) && completo(a,a.subIzq(r),altura);
    }
  }
}

/// Ejercicio 7

template <typename T>
bool existeCamino(const Arbin<T>& a, std::list<T> lista){
    return existeCamino(a,a.getRaiz(),lista);
}

template <typename T>
bool existeCamino(const Arbin<T>& a, const typename Arbin<T>::Iterador& r, std::list<T> lista){

if(lista.empty()) return true;
else{
    if(!r.arbolVacio()){
        if(r.observar()==lista.front()){
                lista.pop_front();
                return existeCamino(a,a.subDer(r),lista) || existeCamino(a,a.subIzq(r),lista);
            }
            else return false;
        }
        else return false;
    }
}

/// Ejercicio 8

template <typename T>
bool similar(const Arbin<T>& a, const Arbin<T>& b){
    return similar(a,b,a.getRaiz(),b.getRaiz());
}

template <typename T>
bool similar(const Arbin<T>& a, const Arbin<T>& b, const typename Arbin<T>::Iterador& ra,  const typename Arbin<T>::Iterador& rb){
    if(!ra.arbolVacio() && !rb.arbolVacio()){
           return  similar(a,b,a.subDer(ra),b.subDer(rb)) &&  similar(a,b,a.subIzq(ra),b.subIzq(rb));
    }
    else if(ra.arbolVacio() && rb.arbolVacio()) return true;
    else return false;
}

/****************************************************************************/
/****************************************************************************/
int main(int argc, char *argv[])
{
    Arbin<char> A('t', Arbin<char>('m', Arbin<char>(),
                                        Arbin<char>('f', Arbin<char>(), Arbin<char>())),
                       Arbin<char>('k', Arbin<char>('d', Arbin<char>(), Arbin<char>()),
                                        Arbin<char>()));

    Arbin<char> B('t', Arbin<char>('n', Arbin<char>(),
                                        Arbin<char>('d', Arbin<char>('e', Arbin<char>(), Arbin<char>()),
                                                         Arbin<char>())),
                       Arbin<char>('m', Arbin<char>('f', Arbin<char>(), Arbin<char>()),
                                        Arbin<char>('n', Arbin<char>(), Arbin<char>())));

    Arbin<char> D('t', Arbin<char>('k', Arbin<char>('d', Arbin<char>(), Arbin<char>()),
                                        Arbin<char>()),
                       Arbin<char>('m', Arbin<char>(),
                                        Arbin<char>('f', Arbin<char>(), Arbin<char>())));

    Arbin<char> E('o', Arbin<char>('r', Arbin<char>(),
                                        Arbin<char>('o', Arbin<char>(), Arbin<char>())),
                       Arbin<char>('l', Arbin<char>('a', Arbin<char>(), Arbin<char>()),
                                        Arbin<char>('e', Arbin<char>(), Arbin<char>())));

    Arbin<int> F(2, Arbin<int>(7, Arbin<int>(2, Arbin<int>(), Arbin<int>()),
                                  Arbin<int>(6, Arbin<int>(5, Arbin<int>(), Arbin<int>()),
                                                Arbin<int>(11, Arbin<int>(), Arbin<int>()))),
                    Arbin<int>(5, Arbin<int>(),
                                  Arbin<int>(9, Arbin<int>(),
                                                  Arbin<int>(4, Arbin<int>(), Arbin<int>()))));

    Arbin<int> Y(7, Arbin<int>(5, Arbin<int>(2, Arbin<int>(), Arbin<int>()),
                                  Arbin<int>(6, Arbin<int>(), Arbin<int>())),
                    Arbin<int>(9, Arbin<int>(8,Arbin<int>(), Arbin<int>()),
                                  Arbin<int>(10, Arbin<int>(),Arbin<int>())));

    ABB<int> BB6, BB7, BB61;



    // NUMERO HOJAS //
    cout << "Num. hojas del arbol B: " << numHojas(B) << endl;
    cout << "Num. hojas del arbol A: " << numHojas(A) << endl;
    cout << "Num. hojas del arbol E: " << numHojas(E) << endl << endl;

    // COPIA SIMETRICA //
    Arbin<char> C = simetrico(B);
    cout << "Recorrido en inorden del arbol B: " << endl;
    inorden(B, B.getRaiz());
    cout << endl << "Recorrido en inorden del arbol C: " << endl;
    inorden(C, C.getRaiz());
    cout << endl << endl;

    Arbin<int> X = simetrico(F);
    cout << "Recorrido en inorden del arbol F: " << endl;
    inorden(F, F.getRaiz());
    cout << endl << "Recorrido en inorden del arbol X: " << endl;
    inorden(X, X.getRaiz());
    cout << endl << endl;

    // RECORRIDO EN ZIG-ZAG //

    cout << "Recorrido en zigzag I de B:\n";
    recorridoZigzag(B, 'I');
    cout << endl;
    cout << "Recorrido en zigzag D de C:\n";
    recorridoZigzag(C, 'D');
    cout << endl << endl;

    // COMPENSADO //
    cout << "Esta A compensado?:";
    cout << (compensado(A) ? " SI" : " NO") << endl;
    cout << "Esta B compensado?:";
    cout << (compensado(B) ? " SI" : " NO") << endl;
    cout << "Esta E compensado?:";
    cout << (compensado(E) ? " SI" : " NO") << endl;
    cout << "Esta F compensado?:";
    cout << (compensado(F) ? " SI" : " NO") << endl << endl;

    // PALABRAS DE UN ARBOL //
    cout << "PALABRAS DE A:\n";
    palabras(E);
    cout << "PALABRAS DE B:\n";
    palabras(B);
    cout << "PALABRAS DE F:\n";
    palabras(F);
    cout << endl;

    // SIGUIENTE MAYOR
    BB6.insertar(8); BB6.insertar(3); BB6.insertar(10); BB6.insertar(1); BB6.insertar(6);
    BB6.insertar(14); BB6.insertar(4); BB6.insertar(7); BB6.insertar(13);
    try
    {
        cout << "Siguiente mayor en BB6 de 5: " << siguienteMayor(BB6, 5) << endl;
        cout << "Siguiente mayor en BB6 de 8: " << siguienteMayor(BB6, 8) << endl;
        cout << "Siguiente mayor en BB6 de 13: " << siguienteMayor(BB6, 13) << endl;
        cout << "Siguiente mayor en BB6 de 14: " << siguienteMayor(BB6, 14) << endl;
    }
    catch(const NoHaySiguienteMayor& e)
    {
        cout << e.Mensaje() << endl << endl;
    }

    BB61.insertar(9); BB61.insertar(7); BB61.insertar(10); BB61.insertar(21); BB61.insertar(6);
    BB61.insertar(2); BB61.insertar(8); BB61.insertar(12); BB61.insertar(16);
    try
    {
        cout << "Siguiente mayor en BB6 de 5: " << siguienteMayor(BB61, 5) << endl;
        cout << "Siguiente mayor en BB6 de 8: " << siguienteMayor(BB61, 8) << endl;
        cout << "Siguiente mayor en BB6 de 16: " << siguienteMayor(BB61, 16) << endl;
        cout << "Siguiente mayor en BB6 de 21: " << siguienteMayor(BB61, 21) << endl;
    }
    catch(const NoHaySiguienteMayor& e)
    {
        cout << e.Mensaje() << endl << endl;
    }

    //POSICION INORDEN //
    BB7.insertar(5); BB7.insertar(1); BB7.insertar(3); BB7.insertar(8); BB7.insertar(6);
    cout << "Posicion Inorden en BB7 de 1: ";
    cout << posicionInorden(BB7, 1);
    cout << endl << "Posicion Inorden en BB7 de 3: ";
    cout << posicionInorden(BB7, 3);
    cout << endl << "Posicion Inorden en BB7 de 8: ";
    cout << posicionInorden(BB7, 8);
    cout << endl << "Posicion Inorden en BB7 de 7: ";
    cout << posicionInorden(BB7, 7);
    cout << endl << endl;

    // SUMA CAMINO
    cout << "Hay un camino de suma 26 en F?:";
    cout << (haySumaCamino(F, 26) ? " SI" : " NO") << endl;
    cout << "Hay un camino de suma 9 en F?:";
    cout << (haySumaCamino(F, 9) ? " SI" : " NO") << endl;
    cout << "Hay un camino de suma 20 en F?:";
    cout << (haySumaCamino(F, 20) ? " SI" : " NO") << endl << endl;

    cout << "Hay un camino de suma 26 en BB7?:";
    cout << (haySumaCamino(BB7, 26) ? " SI" : " NO") << endl;
    cout << "Hay un camino de suma 9 en BB7?:";
    cout << (haySumaCamino(BB7, 9) ? " SI" : " NO") << endl;
    cout << "Hay un camino de suma 20 en BB7?:";
    cout << (haySumaCamino(BB7, 20) ? " SI" : " NO") << endl << endl;

    //IDENTICOS
    cout << "El arbol A y A son iguales?: ";
    cout << (identicos(A,A) ? " SI " : " NO ") << endl;

    cout << "El arbol A y B son iguales?: ";
    cout << (identicos(A,B) ? " SI " : " NO ") << endl << endl;

    //SUMANODOS

    cout << " La suma de los nodos de F es: ";
    cout << sumanodos(F) << endl << endl;

    // NIVEL
    cout << " Los elementos del nivel 3 del arbol B:  ";
    nivel(B,3);
    cout << endl << " Los elementos del nivel 2 del arbol E:  ";
    nivel(E,2);
    cout << endl;


    // ARBOL COMPLETO

    cout << "El arbol F esta completo?: ";
    cout << (completo(F) ? "SI" : "NO") << endl;
    cout << "El arbol A esta completo?: ";
    cout << (completo(A) ? "SI" : "NO") << endl;
    cout << "El arbol B esta completo?: ";
    cout << (completo(B) ? "SI" : "NO") << endl;
    cout << "El arbol Y esta completo?: ";
    cout << (completo(Y) ? "SI" : "NO") << endl << endl;

    // LISTA
    std::list<char> lista;
    lista.push_back('o');
    lista.push_back('l');
    lista.push_back('a');
    cout << "Existe el camino ola?: ";
    cout << (existeCamino(E,lista) ? " SI " : " NO ") << endl << endl;

    lista.push_back('o');
    lista.push_back('l');
    lista.push_back('i');
    cout << "Existe el camino oli?: ";
    cout << (existeCamino(E,lista) ? " SI " : " NO ") << endl << endl;

    // SIMILAR
    cout << "El arbol A y A son similares?: ";
    cout << (similar(A,A) ? " SI " : " NO ") << endl;

    cout << "El arbol A y B son similares?: ";
    cout << (similar(A,B) ? " SI " : " NO ") << endl << endl;

////    ARBOL BUSQUEDA
//
//    cout << "El arbol A es de Busuqeda?: ";
//    cout << (esBusqueda(A) ? "SI" : "NO") << endl;
//    cout << "El arbol F es de Busuqeda?: ";
//    cout << (esBusqueda(F) ? "SI" : "NO") << endl;
//    cout << "El arbol Y es de Busuqeda?: ";
//    cout << (esBusqueda(Y) ? "SI" : "NO") << endl << endl;
////
//////      POSICION POSORDEN //
////    BB7.insertar(5); BB7.insertar(1); BB7.insertar(3); BB7.insertar(8); BB7.insertar(6);
////    cout << "Posicion Posorden en BB7 de 1: ";
////    cout << posicionPosorden(BB7, 1);
////    cout << endl << "Posicion Posorden en BB7 de 3: ";
////    cout << posicionPosorden(BB7, 3);
////    cout << endl << "Posicion Posorden en BB7 de 8: ";
////    cout << posicionPosorden(BB7, 8);
////    cout << endl << "Posicion Posorden en BB7 de 7: ";
////    cout << posicionPosorden(BB7, 7);
////    cout << endl << endl;
////
////    INTERVALO DESCENDENTE
////
////    cout << "Intervalo descendente BB6: " << endl;
////    intervaloDescendente(BB6, 4, 10);
////    cout << endl;
////    cout << "Intervalo descendente BB7: " << endl;
////    intervaloDescendente(BB7,2,7);
////    cout << endl << endl;
////
////    ARBOL HUECO
////
////    cout << "El arbol A esta Hueco?: "<<endl;
////    cout << (estaHueco(A) ? "SI" : "NO" ) << endl;
////    cout << "El arbol B esta Hueco?: "<<endl;
////    cout << (estaHueco(B) ? "SI" : "NO" ) << endl;
////    cout << "El arbol E esta Hueco?: "<<endl;
////    cout << (estaHueco(E) ? "SI" : "NO" ) << endl;
////    cout << "El arbol D esta Hueco?: "<<endl;
////    cout << (estaHueco(D) ? "SI" : "NO" ) << endl << endl;

    system("PAUSE");
    return 0;
}
