#include <iostream>
#include <cstdlib>
#include "grafo.h"
#include "conjunto.h"
#include <queue>
#include <sstream>
#include <map>


using namespace std;

//Ejercicio 1

//Escribir una funci�n que devuelva el v�rtice de un grafo con el m�ximo �coste total de salida� (suma de
//los costes de las aristas que parten de un v�rtice). Si hubiera varios v�rtices con dicho m�ximo coste,
//devolver cualquiera de ellos:
//    template <typename T>
//    T verticeMaxCoste(const Grafo<T, float>& G)
//En el grafo representado en la figura del ejercicio 2, la funci�n podr�a devolver el v�rtice 1 o el 5, ya
//que ambos tienen el mismo coste total de salida (6), que es el m�ximo.

template <typename T>
T verticeMaxCoste(const Grafo<T, float>& G)
{
    float maximo=0;
    map<T,float> DVerticesMax;

    Conjunto<Vertice<T> > cv=G.vertices();
    Conjunto<Arista<T , float> > ca = G.aristas();

    while(!cv.esVacio()){
        DVerticesMax[cv.quitar().getObj()]=0;
    }

    while(!ca.esVacio()){
        Arista<T,float> a = ca.quitar();
        DVerticesMax[a.getOrigen()]+=a.getEtiqueta();
    }

    T aux = DVerticesMax.begin()->first;

    for(typename map<T,float>::iterator it=DVerticesMax.begin(); it!=DVerticesMax.end(); it++){
        if(it->second > maximo){
            aux=it->first;
            maximo=it->second;
        }
    }
    return aux;
}


//Ejercicio 2

//Dado un grafo, escribe una funci�n que muestre por pantalla los v�rtices inaccesibles del mismo. Se
//entiende por v�rtice inaccesible aqu�l al que no se puede acceder desde ning�n otro v�rtice del grafo.
//Por tanto, cualquier v�rtice que no sea destino de ninguna arista es un v�rtice inaccesible del grafo.
//    template <typename T, typename U>
//    void inaccesibles(const Grafo<T, U>& G)
//En el grafo representado en la figura siguiente, los v�rtices inaccesibles son el 2 y el 5.

template <typename T, typename U>
void inaccesibles(const Grafo<T, U>& G)
{
    Conjunto <Vertice<T> > cv = G.vertices();
    Conjunto <Vertice<T> > Vertices_D;
    Conjunto <Vertice<T> > Vertices_NO;
    Conjunto <Arista<T,float> > ca = G.aristas();

    while(!ca.esVacio()){
        Vertices_D.anadir(ca.quitar().getDestino());
    }

    while(!cv.esVacio()){
        Vertice<T> w = cv.quitar();
        if(!Vertices_D.pertenece(w))
            Vertices_NO.anadir(w);
    }

    while(!Vertices_NO.esVacio())
        cout << Vertices_NO.quitar().getObj() << " ";
}


// Ejercicio 3

//Dado un grafo dirigido (G) y dos v�rtices (vo, vd), escribe una funci�n que indique si existe o no un
//camino entre dichos v�rtices.
//    template <typename T, typename U>
//    bool caminoEntreDos(const Grafo<T, U>& G, const T& vo, const T& vd)
//NOTA: Resolver este ejercicio con un algoritmo NO recursivo, y haciendo uso del TAD Cola
//proporcionado por la STL (queue).

template <typename T, typename U>
bool caminoEntreDos(const Grafo<T, U>& G, const T& vo, const T& vd)
{
    map<T,bool> DVerticesMax;

    Conjunto<Vertice<T> > cv=G.vertices();

    while(!cv.esVacio()){
        DVerticesMax[cv.quitar().getObj()]=false;
    }

    queue <T> cola;
    T aux;
    cola.push(vo);
    while(!cola.empty()){
        aux=cola.front();
        cola.pop();
        DVerticesMax[aux]=true;
        //cout << "Elemento procesando: " << aux << endl;
        Conjunto <Vertice<T> > _adyancentes = G.adyacentes(aux);
        while(!_adyancentes.esVacio()){
            aux=_adyancentes.quitar().getObj();
            //cout<<"\tElemento a meter en la colsa: " << aux << endl;
            if(DVerticesMax[aux]==false){
                DVerticesMax[aux]=true;
                cola.push(aux);
            }
        }
    }

    if(DVerticesMax[vd]==true) return true;
    else return false;
}


//Ejercicio 4
//Escribir un procedimiento que dados un grafo y un v�rtice del mismo, muestre por pantalla todos los
//caminos que partan de dicho v�rtice y tengan un coste total no superior a un valor dado (maxCoste)
//template <typename T>
//void caminosAcotados(const Grafo<T, float>& G, const T& u, float maxCoste)
//Nota: Para resolver este ejercicio, se recomienda pensar en un algoritmo recursivo
template <typename T>
void caminosAcotados(const Grafo<T, float>& G, const T& u, float maxCoste)
{
    float Coste = 0;
    queue <T> Cola;
    caminosAcotados(G, u, maxCoste, Coste, Cola);
}

template <typename T>
void caminosAcotados(const Grafo<T, float>& G, const T& u, float maxCoste, float Coste, queue<T> Cola){
    if(Coste==maxCoste)
    {
        Cola.push(u);
        while(!Cola.empty()){
            cout << Cola.front()<< " - ";
            Cola.pop();
        }
    }
    else
    {
        float _Coste;
        Conjunto<Arista<T,float> > cA = G.aristas();
        Cola.push(u);
        while(!cA.esVacio()){
            Arista<T,float> _A = cA.quitar();
            if(_A.getOrigen()==u){
                _Coste=_A.getEtiqueta()+Coste;
                if(_Coste<=maxCoste)
                    caminosAcotados(G,_A.getDestino(),maxCoste,_Coste,Cola);
            }
        }
    }
}

//Ejercicio 5
//Escribir una funci�n que devuelva un v�rtice outConectado de un grafo. Se considera que un v�rtice
//est� outConectado si tiene un mayor n�mero de de conexiones de salida que de entrada. Si hubiera
//varios v�rtices outConectados, la funci�n puede devolver cualquiera de ellos.
//template <typename T, typename U>
//T outConectado(const Grafo<T, U>& G)
//Por ejemplo, en el grafo del ejercicio 2, el v�rtice 1 estar�a outConectado.
//Nota: Para resolver este ejercicio, se puede suponer que al menos hay un v�rtice outconectado
template <typename T, typename U>
T outConectado(const Grafo<T, U>& G)
{
    Conjunto<Vertice<T> > cv=G.vertices();
    T aux;
    bool _outCone=false;
    while(!cv.esVacio() && !_outCone){
        aux = cv.quitar().getObj();
        Conjunto<Vertice<T> > _adyacentes = G.adyacentes(aux);
        Conjunto<Vertice<T> > _antecesores = G.antecesores(aux);
        if(_adyacentes.cardinalidad()>_antecesores.cardinalidad())
            _outCone = true;
    }
    return aux;
}


//Ejercicio 6
//Escribir un procedimiento que muestre por pantalla el recorrido en profundidad a partir de un grafo y un
//v�rtice dados, pero sin utilizar ninguna estructura auxiliar (pila o cola). Se trata de escribir un algoritmo
//recursivo.
template <typename T, typename U>
void recorrido_profundidad(const Grafo<T, U>& G, const T& v)
{
    map<T,bool> _recorridos;
    Conjunto<Vertice<T> > cv=G.vertices();
    while(!cv.esVacio()){
        _recorridos[cv.quitar().getObj()]=false;
    }
    recorrido_profundidad(G, v, _recorridos);
}

template <typename T, typename U>
void recorrido_profundidad(const Grafo<T,U>& G, const T& v, map<T,bool>& _recorridos)
{
    _recorridos[v]=true;
    T aux;
    Conjunto<Vertice<T> > _adyacentes=G.adyacentes(v);
    cout << v << " ";
    while(!_adyacentes.esVacio()){
        aux=_adyacentes.quitar().getObj();
        if(_recorridos[aux]==false)
            recorrido_profundidad(G,aux,_recorridos);
    }
}

//********************************************************************//
int main()
{
    Grafo<int, float> G(7);
    for (int i = 1; i <= 7; i++) G.insertarVertice(i);
    G.insertarArista(2, 1, 4);
    G.insertarArista(1, 3, 3);
    G.insertarArista(1, 4, 2);
    G.insertarArista(1, 6, 1);
    G.insertarArista(6, 4, 5);
    G.insertarArista(4, 7, 3);
    G.insertarArista(5, 1, 6);

    Grafo<string, float> H(7);
    H.insertarVertice("Huelva"); H.insertarVertice("Lepe"); H.insertarVertice("Niebla");
    H.insertarVertice("Mazagon"); H.insertarVertice("Almonte"); H.insertarVertice("Aljaraque");
    H.insertarVertice("Matalasca�as");
    H.insertarArista("Lepe", "Huelva", 4);
    H.insertarArista("Huelva", "Niebla", 3);
    H.insertarArista("Huelva", "Mazagon", 2);
    H.insertarArista("Huelva", "Aljaraque", 1);
    H.insertarArista("Mazagon", "Almonte", 3);
    H.insertarArista("Mazagon", "Matalasca�as", 4);
    H.insertarArista("Aljaraque", "Mazagon", 5);
    H.insertarArista("Almonte", "Huelva", 6);


    cout << " Vertice de maximo coste en G: " << verticeMaxCoste(G) << endl;
    cout << " Vertice de maximo coste en H: " << verticeMaxCoste(H) << endl;

    cout << endl << " Vertices inaccesibles en G: ";
    inaccesibles(G);
    cout << endl;

//    cout << endl << " Vertices inaccesibles en H: ";
//    inaccesibles(H);
//    cout << endl;

    cout << endl << " Camino entre Dos en H de Lepe a Almonte: ";
    cout << (caminoEntreDos(H, string("Lepe"), string("Almonte")) ? " SI " : " NO ") << endl;
    cout << endl << " Camino entre Dos en H de Aljaraque a Lepe: ";
    cout << (caminoEntreDos(H, string("Aljaraque"), string("Lepe")) ? " SI " : " NO ") << endl;

    cout << endl << " Caminos acotados en G a coste 9 desde el vertice 2:" << endl;
    caminosAcotados(G, 2, 9);

    cout << endl << endl << " Vertice outConectado en G: " << outConectado(G);
    cout << endl << " Vertice outConectado en H: " << outConectado(H);

    cout << endl << endl << " Recorrido en profundidad de H desde el vertice Huelva:  ";
    recorrido_profundidad(H, string("Huelva"));
    cout << endl << endl;


    system("PAUSE");
    return EXIT_SUCCESS;
}
